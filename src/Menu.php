<?php

namespace Xsoft\MenuHelper;

class Menu
{
    protected $items;


    public function addItems(...$items)
    {
        foreach($items as $item){
            $this->items[] = $item;
        }
    }

    public function addLabelItem($label){
        $this->items[] = $label;
    }

    public function getItems()
    {
        return $this->items;
    }

    public function output()
    {
        return view('menu::menu', ['elements' => $this->items]);
    }

    protected function makeMenu($params)
    {
        foreach ($params as $param) {
            if($param instanceof MenuItem){
                $this->addItems($param);
            }else{
                $this->addLabelItem($param);
            }
        }
        return $this;
    }

    public static function __callStatic($method, $arguments)
    {
        if($method = 'make'){
            $menu = new Menu();
            $menu->makeMenu($arguments);
            return $menu;
        }
    }
}

<?php

namespace Xsoft\MenuHelper;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

class MenuItem
{
    protected $name;
    protected $icon;
    protected $route;
    protected $class;
    protected $children = [];
    protected $activeOn = [];
    protected $roles = [];
    protected $permissions = [];

    public function __construct($name, $route = null, $icon = null, ...$children)
    {
        $this->name = $name;
        $this->route = $route;
        $this->icon = $icon;
        foreach ($children as $child) {
            $this->addChild($child);
        }
    }

    public function name($name)
    {
        $this->name = $name;
        return $this;
    }

    public function route($route)
    {
        $this->route = $route;
        return $this;
    }

    public function icon($icon)
    {
        $this->icon = $icon;
        return $this;
    }

    public function class($class)
    {
        $this->class = $class;
        return $this;
    }

    public function activeOn($activeOn)
    {
        $this->activeOn = $activeOn;
        return $this;
    }

    public function addChild($child)
    {
        $this->children[] = $child;
        return $this;
    }

    public function roles($roles)
    {
        $this->roles = $roles;
        return $this;
    }

    public function permissions($permissions)
    {
        $this->permissions = $permissions;
        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    function getIcon()
    {
        return $this->icon;
    }

    function getRoute()
    {
        return $this->route;
    }

    function getClass()
    {
        return $this->class;
    }


    function getChildren()
    {
        return $this->children;
    }

    function getActiveOn()
    {
        return $this->activeOn;
    }


    public function checkRoutes()
    {
        $name = url()->full();
        if ($this->getRoute() == $name) {
            return true;
        }
        $children = $this->getChildren();
        if (is_array($children)) {
            foreach ($children as $child) {
                if ($child->checkRoutes($name)) {
                    return true;
                }
            }
        }
        $activeOn = $this->getActiveOn();
        if (is_array($activeOn)) {
            foreach ($activeOn as $ao) {
                if ($ao == $name) {
                    return true;
                } else if (!empty(preg_match('#' . $ao . '#', $name))) {
                    return true;
                }
            }
        }
        return false;
    }

    public function checkRole()
    {
        if ($this->route === null && empty($this->children)) {
            return true;
        }
        if (!empty($this->roles) || !empty($this->permissions)) {
            foreach ($this->roles as $role) {
                if (Auth::user()->hasRole($role) || Auth::user()->hasRole('superadmin')) {
                    return true;
                }
            }
            foreach ($this->permissions as $permission) {
                if (Auth::user()->can($permission) || Auth::user()->hasRole('superadmin')) {
                    return true;
                }
            }
        } else {
            if (!$this->route) {
                $children = $this->getChildren();
                if (is_array($children)) {
                    foreach ($children as $child) {
                        if ($child->checkRole()) {
                            return true;
                        }
                    }
                }
                return false;
            }
            $route = app('router')->getRoutes()->match(app('request')->create($this->route));
            $routeCollection = Route::getRoutes();
            foreach ($routeCollection as $rc) {
                if ($rc->getName() == $route->getName()) {
                    foreach ($rc->middleware() as $key => $middleware) {
                        if (str_contains($middleware, 'role_or_permission')) {
                            $rolesOrPermissionsArray = explode(':', $middleware);
                            if (key_exists(1, $rolesOrPermissionsArray)) {
                                $roleOrPermissions = explode('|', $rolesOrPermissionsArray[1]);
                                foreach ($roleOrPermissions as $roleOrPermission) {
                                    if (Auth::user()->hasRole($roleOrPermission) || Auth::user()->can($roleOrPermission) && !Auth::user()->hasRole('superadmin')) {
                                        return true;
                                    }
                                }
                            }
                        } else if (str_contains($middleware, 'permission')) {
                            $permissionsArray = explode(':', $middleware);
                            if (key_exists(1, $permissionsArray)) {
                                $permissions = explode('|', $permissionsArray[1]);
                                foreach ($permissions as $permission) {
                                    if (Auth::user()->can($permission) || Auth::user()->hasRole('superadmin')) {
                                        return true;
                                    }
                                }
                            }
                        } else if (str_contains($middleware, 'role')) {
                            $rolesArray = explode(':', $middleware);
                            if (key_exists(1, $rolesArray)) {
                                $roles = explode('|', $rolesArray[1]);
                                foreach ($roles as $role) {
                                    if (Auth::user()->hasRole($role) || Auth::user()->hasRole('superadmin')) {
                                        return true;
                                    }
                                }
                            }
                        } else {
                            if ($key == (count($rc->middleware()) - 1)) {
                                return true;
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    public function find($name)
    {
        $return = false;
        if ($this->route == $name) {
            $return = [[$this->name, $this->route]];
        } else {
            foreach ($this->children as $child) {
                $return = $child->find($name);
                if (is_array($return)) {
                    $return[] = [$this->name, $this->route];
                }
            }
        }
        return $return;
    }

    public static function __callStatic($method, $arguments)
    {
        if ($method = 'create') {
            $item = new MenuItem(...$arguments);
            return $item;
        }
    }

}

<?php

namespace Xsoft\MenuHelper;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\View\Compilers\BladeCompiler;

class MenuHelperServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../src/Helpers/' => app_path() . '/Helpers',
        ], 'menuHelper');

        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'menu');
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
    }
}

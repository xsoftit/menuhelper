<?php

namespace App\Helpers;

use Xsoft\MenuHelper\Menu;
use Xsoft\MenuHelper\MenuItem;

class MenuHelper
{
    public static function build(){
        return Menu::make(
            MenuItem::create(__("app.menu.start"), route('dashboard'), 'af-home')
        )->output();
    }
}

@if($element->getName() == 'hr')
    @if($element->checkRole())
        <hr>
    @endif
@elseif(!$element->getRoute() && !$element->getChildren())
    @if($element->checkRole())
        <li class="header">{{$element->getName()}}</li>
    @endif
@else
    @if($element->checkRole())
        @if(!$element->getChildren())
            <li>
                <a class="app-menu__item @if($element->getClass()){{ $element->getClass() }} @endif @if($element->checkRoutes(\Request::route()->getName())) active @endif"
                   href="@if($element->getRoute()){{ $element->getRoute() }} @else # @endif">
                    @if($element->getIcon())<i class="app-menu__icon af {{ $element->getIcon() }}"></i>@endif
                    <span class="app-menu__label">{{ $element->getName() }}</span>
                </a>
            </li>
        @else
            <li class="treeview @if($element->checkRoutes(\Request::route()->getName())) is-expanded @endif">
                <a class="app-menu__item @if($element->getClass()){{ $element->getClass() }} @endif"
                   href="@if($element->getRoute()) {{ $element->getRoute() }} @else # @endif"
                   data-toggle="treeview">
                    @if($element->getIcon())<i class="app-menu__icon af {{ $element->getIcon() }}"></i>@endif
                    <span class="app-menu__label">{{ $element->getName() }}</span>
                    <i class="treeview-indicator af af-angle-right"></i>
                </a>
                <ul class="treeview-menu">
                    @foreach($element->getChildren() as $child)
                        @include('menu::item',['element' => $child])
                    @endforeach
                </ul>
            </li>
        @endif
    @endif
@endif
